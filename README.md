Introduction
============

PhD thesis template, finalised by Romain Caneill.

The template is based on the work from:
Tore Birkeland,
Raymond Nepstad,
Kjersti Birkeland Daae,
Stephan Kral,
Isabelle Giddy,
Martin Mohrmann.

This is not an official document.


Compile
=======

`latexmk -pdf thesis.tex`

You can clean up the bibfiles and remove abstract and file by using:

```
bibtool -s --sort.format='{%s($key)}' -i references.bib -o references.bib
bibtool -r remove-abs.rsc references.bib -o references.bib
```

License
=======

* The template has MIT license
* The logos are copytight of Gothenburg University, and only provided as placeholder
